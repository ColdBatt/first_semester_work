﻿namespace ShellSort;

public static class ShellSorter
{
    public static void SortCollection(int[] elements, out int iterations)
    {
        iterations = 0;  // iterations counter
        for (var gap = elements.Length / 2; gap > 0; gap /= 2)
        for (var i = gap; i < elements.Length; i++)
        {
            var temp = elements[i];
            int j;

            for (j = i; j >= gap && elements[j - gap].CompareTo(temp) == 1; j -= gap)
            {               
                elements[j] = elements[j - gap];
                iterations++;
            }

            elements[j] = temp;
        }
    }
}