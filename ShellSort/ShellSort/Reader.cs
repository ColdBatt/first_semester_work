﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using CsvHelper;


namespace ShellSort;

public class Reader
{
    public List<int[]> Values;

    public Reader(string path)
    {
        Values = new();
        
        using (StreamReader streamReader = new StreamReader(path))
        {
            while (!streamReader.EndOfStream)
            {
                var line = streamReader.ReadLine()?
                    .Split(';')
                    .Select(x => int.Parse(x))
                    .ToArray();
                
                Values?.Add(line);
            }
        }
    }
}