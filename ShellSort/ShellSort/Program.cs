﻿using System.Diagnostics;

namespace ShellSort;

public static class Program
{
    public static void Main(string[] args)
    {
        var reader = new Reader("C:\\Users\\anton\\PycharmProjects\\SeleniumPracticum\\input_values.csv");
        var writer = new StreamWriter("C:\\Users\\anton\\PycharmProjects\\SeleniumPracticum\\output_values.csv");
        writer.WriteLine("Lenght;Iterations;Time");
        
        var timer = new Stopwatch();
        foreach (var line in reader.Values)
        {
            var testingArray = line[1..];
            timer.Start();
            ShellSorter.SortCollection(testingArray, out int iterations);
            timer.Stop();
            
            writer.WriteLine($"{line[0]};{iterations};{timer.ElapsedMilliseconds}");
        }
        writer.Close();
    }
}

