from random import randint
import csv


input_values = [
    [20 + 80 * i] + [randint(1, 15_000) for _ in range(20 + 80 * i)] for i in range(100)
]


with open('input_values.csv', 'w', newline='') as csv_input:
    writer = csv.writer(csv_input, delimiter=';')
    writer.writerows(input_values)
